# distrobuilder binary

This project builds the latest version of the distrobuilder project.
It can be downloaded automatically from the following URL:

https://gitlab.com/pgregoire-ci/distrobuilder/-/jobs/artifacts/master/raw/distrobuilder?job=build
